const candle = function (arr) {
  return arr.filter(item => item === Math.max.apply(null, arr)).length;
}

module.exports = candle;
