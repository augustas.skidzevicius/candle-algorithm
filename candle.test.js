// birthdayCakeCandles([4, 1, 3, 4]) ==> 2
// birthdayCakeCandles([5, 1, 3, 4, 2]) ==> 1
// birthdayCakeCandles([5, 6, 1, 3, 4, 2, 6, 6]) ==> 3
const candle = require('./candle');

test('lalalalala', () => {
  expect(candle([5, 6, 1, 3, 4, 2, 6, 6])).toBe(3);
});
